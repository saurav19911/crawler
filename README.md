# Crawler
This is a website crawler, which crawls through all links on a page referenced via anchor tags. Only links with the same domain are also crawled further. Sub domains are currently excluded. After a successful crawl it generates a sitemap, which is a json object containing all the pages crawled and all outbound links found on each page.

# Requirements
Crawler has the following requirements
- PHP > 7.1
- cURL: Crawler uses curl_multi_select to asynchronously crawl multiple links on a page
- Crawler is built on the [Lumen](https://lumen.laravel.com/) micro framework. It uses lumen for accessing the command line runner and using it as a web server for tests.
- `symfony/dom-crawler` package is used for parsing the html content.
- `nicmart/tree` is used for a simple tree implementation, used for building the sitemap.

# Installation
Install composer
```sh
curl -sS https://getcomposer.org/installer | php
```
Clone the repository and run composer install to install all required dependencies in the root `crawler` directory
```sh
php composer.phar install
```

# Usage
To crawl a page run the below command on the url
```sh
php artisan crawl https://monzo.com
```
This will start crawling the page, with a default concurrency of 10 (makes 10 requests in parallel to the links found).

To see all other options for the command, please run
```sh
php artisan help crawl
```

This will give the below options
```sh
Description:
  Crawl a url to generate sitemap

Usage:
  crawl [options] [--] <url>

Arguments:
  url                              The url to crawl

Options:
      --concurrency[=CONCURRENCY]  Number of concurrent curl requests [default: "10"]
      --max-depth[=MAX-DEPTH]      Controls the maximum site depth till which a crawl will run
      --max-count[=MAX-COUNT]      Controls the maximum number of unique pages crawled
  -h, --help                       Display this help message
  -q, --quiet                      Do not output any message
  -V, --version                    Display this application version
      --ansi                       Force ANSI output
      --no-ansi                    Disable ANSI output
  -n, --no-interaction             Do not ask any interactive question
      --env[=ENV]                  The environment the command should run under
  -v|vv|vvv, --verbose             Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```

All logs for the application can be found in `/storage/logs/lumen.log` location.

# Sitemap
After a successful crawl, sitemap with the below json structure will be generated
```json
[
    {
        "url": "<url crawled>",
        "links": [
            "<link1>",
            "<link2>"
        ]
    },
    ...
]
```
Here `url` represents the page crawled and `links` represents all the unique outbound links found on the page. All `url` values will contain links in the same domain (representing the pages which have actually been crawled). A sample sitemap output is present in the `sample_sitemap.json` file

# Running Tests
Start the test server, by running the below command from the root directory
```sh
php -S localhost:8000 -t public
```
Run tests via phpunit
```sh
./vendor/bin/phpunit
```