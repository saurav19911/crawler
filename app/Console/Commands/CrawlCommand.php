<?php

namespace App\Console\Commands;

use App\Crawler\Crawler;
use App\Crawler\SiteMapBuilder;
use Illuminate\Console\Command;
use Tree\Visitor\PreOrderVisitor;

class CrawlCommand extends Command
{
    protected $signature = 'crawl
                            {url : The url to crawl}
                            {--concurrency=10 : Number of concurrent curl requests}
                            {--max-depth= : Controls the maximum site depth till which a crawl will run}
                            {--max-count= : Controls the maximum number of unique pages crawled}';

    protected $description = 'Crawl a url to generate sitemap';

    public function handle()
    {
        $url = $this->argument('url');

        $crawler = new Crawler();

        $concurrency = $this->option('concurrency');

        $crawler->setConcurrency($concurrency);

        $maxDepth = $this->option('max-depth') ?? 0;

        $maxCrawlCount = $this->option('max-count') ?? 0;

        $crawler->setMaxDepth($maxDepth);

        $crawler->setMaxCrawlCount($maxCrawlCount);

        $this->info("Starting to crawl $url with concurrency $concurrency");

        $crawler->startCrawl($url);

        $this->info('Building sitemap');

        $siteMap = SiteMapBuilder::make($crawler->getSiteMap());

        $this->info($siteMap->toJson(JSON_PRETTY_PRINT));
    }
}