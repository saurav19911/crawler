<?php

namespace App\Crawler;

use Log;
use Generator;
use Tree\Node\Node;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use App\Crawler\Filter\CrawlFilter;
use App\Crawler\Queues\MemoryQueue;
use App\Crawler\Queues\CrawlerQueue;
use App\Crawler\Filter\CrawlInternalUrls;
use App\Crawler\Handlers\CrawlRequestFailedHandler;
use App\Crawler\Handlers\CrawlRequestSuccessHandler;

class Crawler
{
    protected static $client_options = [
        RequestOptions::CONNECT_TIMEOUT => 10,
        RequestOptions::TIMEOUT         => 10,
        RequestOptions::ALLOW_REDIRECTS => true,
    ];

    /**
     * Number of concurrent HTTP requests to make
     * @var int
     */
    protected $concurrency;

    /**
     * Crawl queue instance to use
     * @var App\Crawler\CrawlerQueue
     */
    protected $crawlQueue;

    /**
     * Site map tree representation
     * @var [type]
     */
    protected $siteMap;

    /**
     * Number of crawled urls
     * @var integer
     */
    protected $crawledCount = 0;

    /**
     * Maximum crawl count limit
     * @var integer | null
     */
    protected $maxCrawlCount = 0;

    /**
     * Maximum tree depth limit
     * @var integer | null
     */
    protected $maxDepth = 0;

    public function __construct(int $concurrency = 100)
    {
        $this->client = new Client(self::$client_options);

        $this->concurrency = $concurrency;

        $this->crawlQueue = new MemoryQueue();

        $this->crawlFilter = new CrawlInternalUrls();
    }

    public function getConcurrency()
    {
        return $this->concurrency;
    }

    public function setConcurrency(int $concurrency)
    {
        $this->concurrency = $concurrency;
    }

    public function getMaxDepth()
    {
        return $this->maxDepth;
    }

    public function setMaxDepth($maxDepth)
    {
        $this->maxDepth = $maxDepth;
    }

    public function getMaxCrawlCount()
    {
        return $this->maxCrawlCount;
    }

    public function setMaxCrawlCount($maxCrawlCount)
    {
        $this->maxCrawlCount = $maxCrawlCount;
    }

    public function getCrawlFilter()
    {
        return $this->setCrawlFilter;
    }

    public function setCrawlFilter(CrawlFilter $crawlFilter)
    {
        $this->crawlFilter = $crawlFilter;
    }

    public function getCrawlQueue()
    {
        return $this->crawlQueue;
    }

    public function setCrawlQueue(CrawlerQueue $queue)
    {
        $this->crawlQueue = $queue;
    }

    public function getSiteMap()
    {
        return $this->siteMap;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function startCrawl(string $baseUrl)
    {
        $baseUrl = $this->normalizeUrl($baseUrl);

        $this->siteMap = new Node((string) $baseUrl);

        $this->baseUrl = new Url($baseUrl);

        $this->crawlFilter->setBaseUrl($this->baseUrl);

        $this->pushToQueue($this->baseUrl);

        $this->processCrawlQueue();
    }

    public function pushToQueue(Url $url)
    {
        if ($this->crawlFilter->shouldCrawl($url) === false)
        {
            Log::info('crawl filter condition failed', ['url' => (string) $url->getUrl()]);
            return;
        }

        if ($this->crawlQueue->has($url) === true)
        {
            Log::info('url already crawled', ['url' => (string) $url->getUrl()]);
            return;
        }

        if ($this->reachedMaxCrawlCount())
        {
            Log::info('max crawl count reached', [
                'url'             => (string) $url->getUrl(),
                'crawled_count'   => $this->crawled_count,
                'max_crawl_count' => $this->maxCrawlCount,
            ]);
            return;
        }

        $this->crawledCount++;

        $this->crawlQueue->add($url);
    }

    public function addToSiteMap(Url $url, Node $node = null)
    {
        $node = $node ?? $this->siteMap;

        $currentUrl = $url->getUrl();
        $parentUrl = $url->getParentUrl();

        $returnNode = null;

        if ($node->getValue() === (string) $parentUrl)
        {
            $newNode = new Node((string) $currentUrl);

            $node->addChild($newNode);

            return $newNode;
        }

        foreach ($node->getChildren() as $currentNode)
        {
            $returnNode = $this->addToSiteMap($url, $currentNode);

            if ($returnNode !== null)
            {
                return $returnNode;
            }
        }
    }

    protected function processCrawlQueue()
    {
        while ($this->crawlQueue->hasPendingUrls() === true)
        {
            $pool = new Pool($this->client, $this->getNextLinkToCrawl(), [
                'concurrency' => $this->concurrency,
                'options'     => $this->client->getConfig(),
                'fulfilled'   => new CrawlRequestSuccessHandler($this),
                'rejected'    => new CrawlRequestFailedHandler($this)
            ]);

            $promise = $pool->promise();

            $promise->wait();
        }
    }

    protected function getNextLinkToCrawl(): Generator
    {
        while ($this->crawlQueue->hasPendingUrls() === true)
        {
            $crawlUrl = $this->crawlQueue->getFirstPendingUrl();

            if ($this->crawlQueue->hasAlreadyBeenProcessed($crawlUrl) === true)
            {
                Log::info('crawl url already processed', ['url' => (string) $crawlUrl->getUrl()]);
                continue;
            }

            $this->crawlQueue->markAsProcessed($crawlUrl);

            yield $crawlUrl->getId() => new Request('GET', $crawlUrl->getUrl());
        }
    }

    protected function reachedMaxCrawlCount(): bool
    {
        return ($this->maxCrawlCount > 0) ? ($this->crawledCount >= $this->maxCrawlCount) : false;
    }

    protected function normalizeUrl(string $baseUrl): Uri
    {
        $baseUrl = new Uri($baseUrl);

        if ($baseUrl->getScheme() === '') {
            $baseUrl = $baseUrl->withScheme('http');
        }

        if ($baseUrl->getPath() === '') {
            $baseUrl = $baseUrl->withPath('/');
        }

        return $baseUrl;
    }
}