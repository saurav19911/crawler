<?php

namespace App\Crawler\Filter;

use App\Crawler\Url;

abstract class CrawlFilter
{
    abstract public function shouldCrawl(Url $url): bool;
}