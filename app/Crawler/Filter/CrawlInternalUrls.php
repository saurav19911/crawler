<?php

namespace App\Crawler\Filter;

use App\Crawler\Url;
use App\Crawler\Crawler;

class CrawlInternalUrls extends CrawlFilter
{
    protected $baseUrl;

    public function setBaseUrl(Url $url)
    {
        $this->baseUrl = $url;
    }

    public function shouldCrawl(Url $crawlUrl): bool
    {
        $baseUrlHost = $this->baseUrl->getUrl()->getHost();

        $crawlUrlHost = $crawlUrl->getUrl()->getHost();

        return $baseUrlHost === $crawlUrlHost;
    }
}