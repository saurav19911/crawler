<?php

namespace App\Crawler\Handlers;

use Log;
use App\Crawler\Crawler;
use GuzzleHttp\Exception\RequestException;

class CrawlRequestFailedHandler
{
    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    public function __invoke(RequestException $exception, $index)
    {
        $crawlUrl = $this->crawler->getCrawlQueue()->getUrlById($index);

        $responseStatusCode = ($exception->hasResponse() === true) ?
            $exception->getResponse()->getStatusCode() :
            null;

        Log::error('request to url failed', [
            'url'         => (string) $crawlUrl->getUrl(),
            'status_code' => $responseStatusCode,
        ]);
    }
}