<?php

namespace App\Crawler\Handlers;

use Log;
use Tree\Node\Node;
use App\Crawler\Url;
use App\Crawler\Crawler;
use GuzzleHttp\Psr7\Uri;
use InvalidArgumentException;
use Illuminate\Support\Collection;
use Psr\Http\Message\UriInterface;
use Symfony\Component\DomCrawler\Link;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class CrawlRequestSuccessHandler
{
    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    public function __invoke(ResponseInterface $response, $id)
    {
        $crawledUrl = $this->crawler->getCrawlQueue()->getUrlById($id);

        Log::info('successfully fetched url', [
            'url' => (string) $crawledUrl->getUrl()
        ]);

        $body = $response->getBody()->getContents();

        $this->addLinksFromHtml($body, $crawledUrl);
    }

    protected function addLinksFromHtml(string $html, Url $parentUrl)
    {
        $allLinks = $this->extractLinks($html, $parentUrl);

        $allLinks->filter(function (Url $url)
        {
            $node = $this->crawler->addToSiteMap($url);

            // If the resulting node level is greater than max depth
            // then that link won't be pushed to the crawl queue
            // even though it might be a valid crawlable link.
            return ($this->maxDepthReached($node) === false);
        })
        ->each(function (Url $url)
        {
            $this->crawler->pushToQueue($url);
        });
    }

    protected function extractLinks(string $html, Url $parentUrl)
    {
        $domCrawler = new DomCrawler($html, $parentUrl->getUrl());

        $links = Collection::make($domCrawler->filterXPath('//a')->links());

        return $links->map(function (Link $link)
        {
            try
            {
                return new Uri($link->getUri());
            }
            catch (InvalidArgumentException $e)
            {
                return;
            }
        })
        ->filter(function ($url)
        {
            return $this->hasVaildScheme($url);
        })
        ->map(function ($url)
        {
            return $this->removeFragment($url);
        })
        ->map(function ($url) use ($parentUrl)
        {
            $crawlUrl = new Url($url, $parentUrl->getUrl());

            return $crawlUrl;
        });
    }

    protected function hasVaildScheme(UriInterface $url)
    {
        return in_array($url->getScheme(), ['http', 'https'], true);
    }

    protected function removeFragment(UriInterface $url)
    {
        // Removes any url # fragments
        return $url->withFragment('');
    }

    protected function maxDepthReached(Node $node): bool
    {
        $maxDepth = $this->crawler->getMaxDepth();

        return ($maxDepth > 0) ? ($node->getDepth() >= $maxDepth) : false;
    }
}