<?php

namespace App\Crawler\Queues;

use App\Crawler\Url;

interface CrawlerQueue
{
    public function add(Url $url);

    public function has(Url $searchUrl): bool;

    public function hasPendingUrls(): bool;

    public function getUrlById($id): Url;

    public function getFirstPendingUrl();

    public function hasAlreadyBeenProcessed(Url $url): bool;

    public function markAsProcessed(Url $url);
}