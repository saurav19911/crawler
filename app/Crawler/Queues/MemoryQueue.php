<?php

namespace App\Crawler\Queues;

use App\Crawler\Url;
use Illuminate\Support\Collection;

class MemoryQueue implements CrawlerQueue
{
    /**
     * List of all crawled urls
     * @var Illuminate\Support\Collection
     */
    protected $allUrls;

    /**
     * List of all urls pending to be crawled
     * @var Illuminate\Support\Collection
     */
    protected $pendingUrls;

    public function __construct()
    {
        $this->allUrls = Collection::make();

        $this->pendingUrls = Collection::make();
    }

    public function getAllUrls()
    {
        return $this->allUrls;
    }

    public function getPendingUrls()
    {
        return $this->pendingUrls;
    }

    public function add(Url $url)
    {
        $this->allUrls->push($url);

        $url->setId($this->allUrls->keys()->last());

        $this->pendingUrls->push($url);
    }

    public function has(Url $searchUrl): bool
    {
        return $this->allUrls->contains(function ($url) use ($searchUrl)
        {
            return ((string) $url->getUrl() === (string) $searchUrl->getUrl());
        });
    }

    public function hasPendingUrls(): bool
    {
        return $this->pendingUrls->isNotEmpty();
    }

    public function getUrlById($id): Url
    {
        return $this->allUrls[$id];
    }

    public function getFirstPendingUrl()
    {
        return $this->pendingUrls->first();
    }

    public function hasAlreadyBeenProcessed(Url $searchUrl): bool
    {
        $presentInPendingUrls = $this->pendingUrls->contains(function ($url) use ($searchUrl)
        {
            return ((string) $url->getUrl() === (string) $searchUrl->getUrl());
        });

        $presentInAllUrls = $this->allUrls->contains(function ($url) use ($searchUrl)
        {
            return ((string) $url->getUrl() === (string) $searchUrl->getUrl());
        });

        return (($presentInPendingUrls === false) and ($presentInAllUrls === true));
    }

    public function markAsProcessed(Url $url)
    {
        $this->pendingUrls = $this->pendingUrls->reject(function ($item) use ($url)
        {
            return ((string) $item->getUrl() === (string) $url->getUrl());
        });
    }
}