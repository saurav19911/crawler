<?php

namespace App\Crawler;

use Tree\Node\Node;
use Illuminate\Support\Collection;

class SiteMapBuilder
{
    public static function make(Node $root)
    {
        $siteMapCollection = Collection::make();

        (new static($siteMapCollection))->build($root);

        return $siteMapCollection;
    }

    public function __construct(Collection $siteMap)
    {
        $this->siteMap = $siteMap;
    }

    protected function build(Node $node)
    {
        if ($node->isLeaf() === true)
        {
            return;
        }

        $pageInfo = $this->getPageInfo($node);

        $this->siteMap->push($pageInfo);

        foreach ($node->getChildren() as $child)
        {
            $this->build($child);
        }

        return $this;
    }

    public function getPageInfo(Node $node)
    {
        $info = Collection::make();

        $info['url'] = $node->getValue();

        $info['links'] = Collection::make();

        foreach ($node->getChildren() as $child)
        {
            $info['links']->push($child->getValue());
        }

        $info['links'] = $info['links']->unique()->values();

        return $info;
    }
}