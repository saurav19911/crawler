<?php

namespace App\Crawler;

use Psr\Http\Message\UriInterface;

class Url
{
    protected $id;

    protected $url;

    protected $parentUrl;

    public function __construct(UriInterface $url, UriInterface $parentUrl = null)
    {
        $this->url = $url;

        $this->parentUrl = $parentUrl;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getParentUrl()
    {
        return $this->parentUrl;
    }
}
