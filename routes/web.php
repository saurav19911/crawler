<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router)
{
    return $router->app->version();
});

$router->get('/link1', function () use ($router)
{
    return response('<html><body><a href="/link2">Internal Link</a><a href="/link3">Another Internal Link</a></body></html>');
});

$router->get('/link2', function () use ($router)
{
    return response('<html><body><a href="http://example.com/">External Link</a><a href="/link1">Link1</a></body></html>');
});

$router->get('/link3', function () use ($router)
{
    return response('<html><body><a href="http://sub.localhost:8000/link">Subdomain link Link</a><a href="/link2">Link2</a></body></html>');
});

