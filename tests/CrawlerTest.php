<?php

use App\Crawler\Crawler;
use Tree\Visitor\YieldVisitor;
use App\Crawler\SiteMapBuilder;

class CrawlerTest extends TestCase
{
    public function testOnlyInternalLinksCrawled()
    {
        $crawler = new Crawler();

        $crawler->startCrawl('http://localhost:8000/link1');

        $crawledUrls = $crawler->getCrawlQueue()->getAllUrls();

        $crawledUrls = $crawledUrls->map(function ($url)
        {
            return (string) $url->getUrl();
        });

        $expectedCrawledUrls = [
            'http://localhost:8000/link1',
            'http://localhost:8000/link2',
            'http://localhost:8000/link3',
        ];

        $this->assertEquals($expectedCrawledUrls, $crawledUrls->toArray());
    }

    public function testAlreadyCrawledLinksNotCrawled()
    {
        $crawler = new Crawler();

        $crawler->startCrawl('http://localhost:8000/link1');

        $crawledUrls = $crawler->getCrawlQueue()->getAllUrls();

        $crawledUrls = $crawledUrls->map(function ($url)
        {
            return (string) $url->getUrl();
        });

        $expectedCrawledUrls = [
            'http://localhost:8000/link1',
            'http://localhost:8000/link2',
            'http://localhost:8000/link3',
        ];

        $this->assertEquals($expectedCrawledUrls, $crawledUrls->toArray());
    }

    public function testSubdomainsNotCrawled()
    {
        $crawler = new Crawler();

        $crawler->startCrawl('http://localhost:8000/link1');

        $crawledUrls = $crawler->getCrawlQueue()->getAllUrls();

        $crawledUrls = $crawledUrls->map(function ($url)
        {
            return (string) $url->getUrl();
        });

        $siteMap = $crawler->getSiteMap();

        $visitor = new YieldVisitor;

        // Outgoing links found on all pages
        $allLinks = array_map(function ($node)
        {
            return (string) $node->getValue();
        }, $siteMap->accept($visitor));

        $expectedCrawledUrls = [
            'http://localhost:8000/link1',
            'http://localhost:8000/link2',
            'http://localhost:8000/link3'
        ];

        $this->assertEquals($expectedCrawledUrls, $crawledUrls->toArray());

        $expectedLinks = [
            'http://example.com/',
            'http://localhost:8000/link1',
            'http://sub.localhost:8000/link',
            'http://localhost:8000/link2'
        ];

        $this->assertEquals($expectedLinks, $allLinks);

        return $crawler;
    }

    public function testSiteMapBuilder()
    {
        $crawler = $this->testSubdomainsNotCrawled();

        $siteMap = $crawler->getSiteMap();

        $siteMapBuilder = SiteMapBuilder::make($siteMap);

        $siteMap = $siteMapBuilder->toArray();

        $expectedSiteMap = [
            [
                'url' => 'http://localhost:8000/link1',
                'links' => [
                    'http://localhost:8000/link2',
                    'http://localhost:8000/link3',
                ]
            ],
            [
                'url' => 'http://localhost:8000/link2',
                'links' => [
                    'http://example.com/',
                    'http://localhost:8000/link1'
                ]
            ],
            [
                'url' => 'http://localhost:8000/link3',
                'links' => [
                    'http://sub.localhost:8000/link',
                    'http://localhost:8000/link2',
                ]
            ]
        ];

        $this->assertEquals($expectedSiteMap, $siteMap);
    }
}